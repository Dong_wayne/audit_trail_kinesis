# Audit Trail

[![Assetic License](http://img.shields.io/badge/license-Assetic%20-blue.svg)](http://www.assetic.com)[![serverless framwork](http://img.shields.io/badge/serverless-framwork%20-red.svg)](https://serverless.com)

## Description

  This is a stream processing part of Audit Trail. It handles the application data and write data to s3 and dynamoDB respectively.

------
## Dependency

| package             |            method              |
|---------------------|--------------------------------|
| go                  | https://golang.org/doc/install |
| AWS                 | github.com/aws                 |
| serverless framwork | npm install serverless -g      |
| uuid                | github.com/satori/go.uuid      |
| bzip2               | github.com/dsnet/compress/bzip2|

------

## <1> Deploy
### 1. Preparation

  1. Install the serverless environment (npm install -g serverless) hint: use (serverless --version) to check
  2. Enter the config and credentials file to set the aws_access_key_id and aws_secret_access_key or export AWS_ACCESS_KEY_ID= your_key_id export AWS_SECRET_ACCESS_KEY= your_access_key
  3. Ensure the policy of role for AWS services access
  4. Create the Execution role and edit trust relationship(API gateway and kinesis)
------
### 2. Deployment

  1. Enter the Audit Trail file

  2. make

  3. serverless deploy

------

## <4> Clean up

  serverless remove --stage dev --region ap-southeast-2
  
  clean all deployed tables 

------

## License

  Copyright 2017 Assetic. All rights reserved.

------
## Contact
  http://www.assetic.com
