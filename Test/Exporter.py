# This is to read the reponse from the cloudwatch
# plot chart for the results
import subprocess
import matplotlib.pyplot as plt
import json
import time
import csv

# Audit trail number of objects
audit_trail_count = []
# Audit trial bucket size bytes
audit_trail_size = []

# input two audit trail files
#read json files

def ReadFiles(audit_trail_count_file,audit_trail_size_file):
    with open(audit_trail_count_file,encoding='utf-16') as f_count:
        data_count = json.load(f_count)
    for i in data_count['Datapoints']:
        count_temp_time = time.strptime(i['Timestamp'], "%Y-%m-%dT%H:%M:%SZ")
        audit_trail_count.append({"date":time.strftime("%Y-%m-%d %H:%M:%S",count_temp_time),"count":i['Average']})
    with open(audit_trail_size_file,encoding='utf-16') as f_size:
        data_size = json.load(f_size)
    for j in data_size['Datapoints']:
        size_temp_time = time.strptime(j['Timestamp'], "%Y-%m-%dT%H:%M:%SZ")
        audit_trail_size.append({"date": time.strftime("%Y-%m-%d %H:%M:%S",size_temp_time), "size": float(j['Average'])/1024/1024})


#split the list
#split the x, y for the plot
#name: required name in the inputlist
def SplitList(input_list,name):
    splittedList = []
    for i in input_list:
        splittedList.append(i[name])
    splittedList.sort()
    return splittedList

#get the day average rate
def getAverage(input_list,name):
    count = 0
    total = 0
    rate = 0
    for i in input_list:
        count +=1
        total = total + float(i[name])
    rate = total/count
    return  rate

# draw line chart
#input the required list, title, x_unit,y_unit
def LineChart(input_list,x,y,x_unit,y_unit,title):
    title = title
    plt.ylabel(y_unit)
    plt.xlabel(x_unit)
    plt.title(title)
    list_x = SplitList(input_list,x)
    list_y = SplitList(input_list,y)
    plt.plot(list_x, list_y)
    plt.show()


# write files to csv
def WriteCSV(input_list,column_x,column_y,filename):
    with open(filename+'.csv','w', newline='') as f:
         fieldnames = [column_x, column_y]
         writer = csv.DictWriter(f, fieldnames=fieldnames)
         writer.writeheader()
         for i in input_list:
             writer.writerow({column_x:i[column_x],column_y:i[column_y]})

#run the exporter to get two reponse files from cloudwatch
subprocess.call(['sh','./exporter.sh'])

ReadFiles('Audit_Trail_count.json','Audit_Trail_size.json')
LineChart(audit_trail_count,'date','count','','Number','Audit Trail Number')
LineChart(audit_trail_size,'date','size','','Megabytes','Audit Trail Size')
WriteCSV(audit_trail_count,'date','count','audit_trail_count')
WriteCSV(audit_trail_size,'date','size','audit_trail_size')