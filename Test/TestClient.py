import requests
import random
import time
import threading
import json
import uuid
# this method generate random data
# and send to Audit Trail system
# data type:
# Client, LogId,SourceType,UserId,UserEmail,DateTimeCreatedAT,SourceId,ChangeDescription,ChangeData,IPAddress,Action,ActionType
URL = "https://wg21d5rk8b.execute-api.ap-southeast-2.amazonaws.com/dev/AuditTrail/AuditTrailStream"
FakeDataList = []
SensorSize = 36
headers = {'Content-type': 'application/json'}
exitFlag = 0

#Initilize thread
class myThread(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter

    def run(self):
        print("Starting " + self.name)
        PostRequest(self.threadID,self.name, self.counter)
        print("Exiting " + self.name)

def PostRequest(threadID,threadName,counter):
    startIndex = (threadID - 1) * counter
    endIndex = threadID * counter
    for i in range(startIndex,endIndex):
        if exitFlag:
            (threading.Thread).exit()
        paramTemp = {'PartitionKey':random.randint(1,4000),'Data':[{'client':FakeDataList[i][0],'logid':FakeDataList[i][1],'sourcetype':FakeDataList[i][2],'userid':FakeDataList[i][3],'useremail':FakeDataList[i][4],'createdat':FakeDataList[i][5],'sourceid':FakeDataList[i][6],'changedescription':FakeDataList[i][7],'changedata':FakeDataList[i][8],'ipaddress':FakeDataList[i][9],'action':FakeDataList[i][10],'actiontype':FakeDataList[i][11],'month':FakeDataList[i][12],'year':FakeDataList[i][13]}]}
        paramJson=json.dumps(paramTemp)
        result = requests.put(URL,data=paramJson,headers=headers)
        if result is not None:
            if "201" in str(result):
               print(str(i+1)+": "+threadName+": "+"post data successfully")
        counter -= 1

#allocate task for each thread
def AllocateTask(Task):
    FakeSensorDataGenerator(Task)
    subTask = int(Task/10)
    thread1 = myThread(1, "Thread-1", subTask)
    thread2 = myThread(2, "Thread-2", subTask)
    thread3 = myThread(3, "Thread-3", subTask)
    thread4 = myThread(4, "Thread-4", subTask)
    thread5 = myThread(5, "Thread-5", subTask)
    thread6 = myThread(6, "Thread-6", subTask)
    thread7 = myThread(7, "Thread-7", subTask)
    thread8 = myThread(8, "Thread-8", subTask)
    thread9 = myThread(9, "Thread-9", subTask)
    thread10 = myThread(10, "Thread-10", subTask)

    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread6.start()
    thread7.start()
    thread8.start()
    thread9.start()
    thread10.start()

def FakeSensorDataGenerator(MaxSize):
        for i in range(MaxSize):
            templist = []
            randomDescription = RandomDescription(random.randint(0,2))
            templist = [RandomClient(random.randint(1, 10)),str(uuid.uuid4()),RandomSource(random.randint(1,15)),str(uuid.uuid4()),Randomemail(random.randint(1, 15)),str(RandomDate("2018-08-01 10:20:30","2018-08-10 11:12:13")),str(uuid.uuid4()),randomDescription,randomDescription,RandomIP(random.randint(1,10)),RandomAction(random.randint(1,10)),RandomActionType(random.randint(1,10)),random.randint(1,12),random.randint(2015,2018)]
            FakeDataList.append(templist)


# random power status
def RandomClient(tempInt):
            if tempInt < 5:
                return "login"
            else:
                return "caclient"
            # if tempInt >= 10 and tempInt<=15:
            #     return "newClient"

# random Source
def RandomSource(tempInt):
            if tempInt< 5:
                return "Assetic"
            if tempInt >=5 and tempInt <10:
                return "Yarra Tram"
            if tempInt> 10:
                return "KFC"

# random email
def Randomemail(tempInt):
            if tempInt< 5:
                return "dwang@assetic.com"
            if tempInt >=5 and tempInt <10:
                return "wangdong8752@gmail.com"
            if tempInt> 10:
                return "dongw3@stuent.unimelb.edu.au"

def RandomIP(tempInt):
            if tempInt<5:
                return "192.168.1.1"
            if tempInt>5:
                return "125.123.256.20"

def RandomActionType(tempInt):
            if tempInt<5:
                return "record"
            else:
                return "tempdata"


def RandomAction(tempInt):
    if tempInt < 5:
        return "motify"
    else:
        return "delete"

# random description
def RandomDescription(tempInt):
            if tempInt == 0:
                return "Data Change Event: PropertyName: Command, OldValue: , NewValue: <?xml version=\"1.0\" encoding=\"utf-16\"?><ReIndexModuleCommand xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Id>c189855b-41cd-41eb-b3fb-836a1f01b15f</Id><Client>preview</Client><RequestedUserId>d36fe8f0-c724-49be-a565-2d5696352e03</RequestedUserId><RequestedUserEmail>nbrent@assetic.com</RequestedUserEmail><RequesterIpAddress>14.203.101.42</RequesterIpAddress><OriginatingUri>https://preview.asseticdev.net/</OriginatingUri><WorkerId>c189855b-41cd-41eb-b3fb-836a1f01b15f</WorkerId><SearchTypes><string>complexassets</string></SearchTypes></ReIndexModuleCommand>"
            else:
                return "Data Change Event: PropertyName: UserId, OldValue: , NewValue: bb974114-9243-41de-9ae2-de7a24e99b2c"
# random date
def RandomDate(start,end):
    startArray = time.strptime(start, "%Y-%m-%d %H:%M:%S")
    endArray = time.strptime(end, "%Y-%m-%d %H:%M:%S")
    startDate = int(time.mktime(startArray))
    endDate = int(time.mktime(endArray))
    randomDate = random.randint(startDate,endDate)
    tempDate = time.localtime(randomDate)
    ranDate = time.strftime("%Y-%m-%d %H:%M:%S",tempDate)
    return ranDate

if __name__ == '__main__':
    AllocateTask(500)
