# this is to find optimal value setting for kinesis trigger(lambda)
# there are two key value: batch size for the rate of output
# memory is for lambda function memory to reduce the exectution time
# lambda pricing depend on execution time and invocation times

import matplotlib.pyplot as plt
import csv

batchSize1 = []
batchSize2 = []
batchSize3 = []
# input is three batch size csv, each batch size have 128,512,1024 memory
# format
# memory  average duration  price
#
#read csv
def readCsv(batchSizeCSV,batchSizeCSV2,batchSizeCSV3):
    batchSize1_list = []
    batchSize2_list = []
    batchSize3_list = []
    with open(batchSizeCSV,encoding='utf-8') as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            batchSize1_list.append(row)
    with open(batchSizeCSV2,encoding='utf-8') as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            batchSize2_list.append(row)
    with open(batchSizeCSV3,encoding='utf-8') as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            batchSize3_list.append(row)
    return batchSize1_list,batchSize2_list,batchSize3_list

# split header and data
def getData(temp_list):
    data_list =[]
    for i in range(1,len(temp_list)):
        data_list.append(temp_list[i])
    return data_list

# split the list of required index column in the list
def splitData(raw_list,list_index):
    req_list = []
    for i in range(0,len(raw_list)):
        req_list.append(raw_list[i][list_index])
    return req_list


# draw line chart
def lineChart(batchSize1list_x,batchSize1list_y,batchSize2list_x,batchSize2list_y,batchSize3list_x,batchSize3list_y):
    title = "BatchSize and Memory"
    plt.plot(batchSize1list_x,batchSize1list_y,label="batchSize 10")
    plt.plot(batchSize2list_x,batchSize2list_y,label="batchSize 50")
    plt.plot(batchSize3list_x, batchSize3list_y, label="batchSize 100")
    # plt.legend(loc = 'lower right')
    plt.xlabel("Memory(mb)")
    # plt.ylabel("Price($/month)")
    plt.ylabel("Average duration(ms)")
    plt.title(title)
    plt.legend(loc="upper left")
    plt.grid(True)
    plt.show()


list1,list2,list3 = readCsv('batchSize1.csv','batchSize2.csv','batchSize3.csv')
batchSize1_x = splitData(getData(list1),0)
batchSize1_y = splitData(getData(list1),1)
batchSize2_x = splitData(getData(list2),0)
batchSize2_y = splitData(getData(list2),1)
batchSize3_x = splitData(getData(list3),0)
batchSize3_y = splitData(getData(list3),1)
lineChart(batchSize1_x,batchSize1_y,batchSize2_x,batchSize2_y,batchSize3_x,batchSize3_y)
