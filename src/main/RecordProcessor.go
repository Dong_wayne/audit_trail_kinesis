package main

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/dsnet/compress/bzip2"
	"github.com/satori/go.uuid"
	"log"
	"os"
	"strings"
	"sync"
	"time"
)

//get region from deployment
var region = os.Getenv("AWS_REGION")
var bucket = "assetic-wayne-audit-trail"

//   stream record data type
type Record struct {
	Client            string `json:"Client"`
	ID                string `json:"ID"`
	Version           string `json:"Version"`
	SourceType        string `json:"SourceType"`
	UserID            string `json:"UserId"`
	SecurityContext   string `json:"SecurityContext"`
	CreatedAt         string `json:"CreatedAt"`
	SourceID          string `json:"SourceId"`
	ChangeDescription string `json:"ChangeDescription"`
	ChangeData        string `json:"ChangeData"`
	IPAddress         string `json:"IPAddress"`
	Action            string `json:"Action"`
	ActionType        string `json:"ActionType"`
	Environment       string `json:"Environment"`
}

type FormatData struct {
	ID                string `json:"id"`
	SourceType        string `json:"sourcetype"`
	Version           string `json:"version"`
	UserID            string `json:"userid"`
	SecurityContext   string `json:"securitycontext"`
	CreatedAt         string `json:"createdat"`
	SourceID          string `json:"sourceid"`
	ChangeDescription string `json:"changedescription"`
	ChangeData        string `json:"changedata"`
	IPAddress         string `json:"ipaddress"`
	Action            string `json:"action"`
	ActionType        string `json:"actiontype"`
}

// xml format
// to convert string to xml
type XMLData struct {
	Id                 string `xml:"Id"`
	Client             string `xml:"Client"`
	RequestedUserId    string `xml:"RequestedUserId"`
	RequestedUserEmail string `xml:"RequestedUserEmail"`
	RequesterIpAddress string `xml:"RequesterIpAddress"`
	OriginatingUri     string `xml:"OriginatingUri"`
	WorkerId           string `xml:"WorkerId"`
	SearchTypes        Type   `xml:"SearchTypes"`
}

type Type struct {
	SearchType string `xml:"string"`
}

// kinesis trigger
func RecordProcessor(ctx context.Context, event events.KinesisEvent) {

	sess := session.New(&aws.Config{
		Region: aws.String(region),
	})

	// save records based on client and env
	var batchArray [][]Record

	for _, j := range event.Records {
		recordArr := []Record{}
		if len(j.Kinesis.Data) > 0 {
			err := json.Unmarshal(j.Kinesis.Data, &recordArr)
			if err != nil {
				log.Println(err)
			}
			if len(recordArr) > 0 {
				index := CheckClient(recordArr[0].Client, recordArr[0].Environment, batchArray)
				if index != 0 {
					for _, item := range recordArr {
						batchArray[index] = append(batchArray[index], item)
					}
				} else {
					batchArray = append(batchArray, recordArr)
				}
			}
		}
	}

	var waitGroup sync.WaitGroup
	// get kinesis stream event record
	for _, i := range batchArray {
		if len(i) > 0 {
			waitGroup.Add(1)
			go WriteFiles(i, sess, &waitGroup)
		}
	}
	waitGroup.Wait()
	log.Println("Upload tasks finished.")
}

// Check the client name with env if in the batchArray
// if yes return the index
// if no return 0 (false) than batchArray will create new array to save client
func CheckClient(clientName string, env string, batchArr [][]Record) int {
	if len(batchArr) > 0 {
		for i := 0; i < len(batchArr); i++ {
			if len(batchArr[i]) > 0 {
				if batchArr[i][0].Client == clientName && batchArr[i][0].Environment == env {
					return i
				}
			}
		}
	}
	return 0
}

//format data
//convernt xml to string for changeDescritionEvent and format date yyyy-mm-dd hh:mm:ss
func DataFormatter(record Record) FormatData {
	formatData := FormatData{}
	// change utf-16 to utf-8
	if strings.Contains(record.ChangeDescription, "<") && strings.Contains(record.ChangeDescription, ">") {
		record.ChangeDescription = strings.Replace(record.ChangeDescription, "utf-16", "utf-8", 1)
		splitByComma := strings.Split(record.ChangeDescription, ",")
		splitData := strings.Split(splitByComma[2], ":")
		var result string
		for i := 1; i < len(splitData); i++ {
			result = result + splitData[i]
		}
		// convert xml to string remove xml structure
		xmlData := XMLData{}
		xml.Unmarshal([]byte(result), &xmlData)
		record.ChangeDescription = splitByComma[0] + splitByComma[1] + " NewValue: Id:" + xmlData.Id + " Client:" + xmlData.Client + " OriginatingUri:" + xmlData.OriginatingUri + " RequestedUserEmail:" + xmlData.RequestedUserEmail + " RequestedUserId:" + xmlData.RequestedUserId + " RequesterIpAddress:" + xmlData.RequesterIpAddress + " SearchTypes:" + xmlData.SearchTypes.SearchType + " WorkerId:" + xmlData.WorkerId
		record.ChangeData = record.ChangeDescription
	}
	//format date
	tempDate, err := time.Parse("2006-01-02T15:04:05Z", record.CreatedAt)
	if err != nil {
		log.Print(err)
	}
	record.CreatedAt = tempDate.Format("2006-01-02 15:04:05")
	formatData.CreatedAt = record.CreatedAt
	formatData.Action = record.Action
	formatData.ActionType = record.ActionType
	formatData.ChangeData = record.ChangeData
	formatData.ChangeDescription = record.ChangeDescription
	formatData.IPAddress = record.IPAddress
	formatData.ID = record.ID
	formatData.SourceID = record.SourceID
	formatData.SourceType = record.SourceType
	formatData.SecurityContext = record.SecurityContext
	formatData.UserID = record.UserID
	formatData.Version = record.Version
	return formatData
}

//  asychronous write files to bucket
func WriteFiles(record []Record, svc *session.Session, waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()
	var result []byte
	rec := Record{}
	rec = record[0]

	// remove the array and format the byte[] data
	for i := 0; i < len(record); i++ {
		if i != 0 {
			comma := make([]byte, 1)
			comma[0] = ','
			result = append(result, comma...)
		}
		data, dataErr := json.Marshal(DataFormatter(record[i]))
		if dataErr != nil {
			log.Println(dataErr)
		}
		result = append(result, data...)
	}
	uid, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	tempDate, err := time.Parse("2006-01-02T15:04:05Z", rec.CreatedAt)
	if err != nil {
		log.Print(err)
	}

	// compress the data
	var zipData bytes.Buffer

	// writer config level
	config := &bzip2.WriterConfig{}
	config.Level = 3

	bz2, err := bzip2.NewWriter(&zipData, config)
	if _, err := bz2.Write(result); err != nil {
		log.Println(err)
	}

	if err := bz2.Close(); err != nil {
		log.Println(err)
	}

	//  partition key path client/env/year/month/day
	path := "/client=" + rec.Client + "/env=" + rec.Environment + "/createdate=" + tempDate.Format("02-01-2006") + "/" + uid.String() + ".json.bz2"

	_, err = s3manager.NewUploader(svc).Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(path),
		Body:   bytes.NewReader(zipData.Bytes()),
	})

	if err != nil {
		log.Println(uid.String() + ": file failed!")
		log.Println(err)
	}
}

func main() {
	lambda.Start(RecordProcessor)
}
