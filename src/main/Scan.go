package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"os"
	"time"
)

// Reponse data
type ResponseData struct {
	Id                string `json "Id"`
	LogId             string `json:"LogId"`
	SourceType        string `json:"SourceType"`
	UserId            string `json:"UserId"`
	UserEmail         string `json:"UserEmail"`
	CreatedAT         int64  `json:"CreatedAT"`
	SourceId          string `json:"SourceId"`
	ChangeDescription string `json:"ChangeDescription"`
	ChangeData        string `json:"ChangeData"`
	IPAddress         string `json:"IPAddress"`
	Action            string `json:"Action"`
	ActionType        string `json:"ActionType"`
	Month             int    `json:"month"`
	Year              int    `json:"year"`
}

// Top Data
type TopData struct {
	LogId             string `json:"LogId"`
	SourceType        string `json:"SourceType"`
	UserId            string `json:"UserId"`
	UserEmail         string `json:"UserEmail"`
	CreatedAT         string `json:"CreatedAT"`
	SourceId          string `json:"SourceId"`
	ChangeDescription string `json:"ChangeDescription"`
	ChangeData        string `json:"ChangeData"`
	IPAddress         string `json:"IPAddress"`
	Action            string `json:"Action"`
	ActionType        string `json:"ActionType"`
	Month             int    `json:"month"`
	Year              int    `json:"year"`
}

// Slice of ResponseData
type SliceTopData struct {
	items []TopData
}

// AWS region and DynamoDB
var region = os.Getenv("AWS_REGION")

var header = map[string]string{
	"Access-Control-Allow-Origin":      "*",
	"Access-Control-Allow-Credentials": "true",
}

// to connect the database and to use get method to get the device with id from database
func Scan(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var tableName = request.PathParameters["TableName"]

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)

	if err != nil {
		return events.APIGatewayProxyResponse{
			Headers:    header,
			Body:       "Internal Server Error," + err.Error(),
			StatusCode: 500,
		}, nil
	}

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	params := &dynamodb.ScanInput{
		TableName: aws.String(tableName),
		Limit:     aws.Int64(20),
	}

	// scan all the item in the table
	result, err := svc.Scan(params)

	if err != nil {
		return events.APIGatewayProxyResponse{
			Headers:    header,
			Body:       "Not Found," + err.Error(),
			StatusCode: 404,
		}, nil
	}

	items := []TopData{}
	topSlice := SliceTopData{items}
	// unmarshal the data from database
	for _, i := range result.Items {
		topData := TopData{}
		responseData := ResponseData{}
		err = dynamodbattribute.UnmarshalMap(i, &responseData)
		topData.Action = responseData.Action
		topData.ActionType = responseData.ActionType
		topData.ChangeData = responseData.ChangeData
		topData.ChangeDescription = responseData.ChangeDescription
		topData.IPAddress = responseData.IPAddress
		topData.LogId = responseData.LogId
		topData.Month = responseData.Month
		topData.SourceId = responseData.SourceId
		topData.SourceType = responseData.SourceType
		topData.UserEmail = responseData.UserEmail
		topData.UserId = responseData.UserId
		topData.Year = responseData.Year
		topData.CreatedAT = time.Unix(responseData.CreatedAT, 0).Format("2006-01-02 15:04:05")
		topSlice.AddItem(topData)
		if err != nil {
			return events.APIGatewayProxyResponse{
				Headers:    header,
				Body:       "Internal server error," + err.Error(),
				StatusCode: 500,
			}, nil
		}
	}
	outPut, err := json.Marshal(topSlice.items)
	if err != nil {
		panic(err)
	}
	return events.APIGatewayProxyResponse{
		Headers:    header,
		Body:       string(outPut),
		StatusCode: 200,
	}, nil
}

// function to additem to slice
func (topSlice *SliceTopData) AddItem(item TopData) []TopData {
	topSlice.items = append(topSlice.items, item)
	return topSlice.items
}

func main() {
	lambda.Start(Scan)
}
