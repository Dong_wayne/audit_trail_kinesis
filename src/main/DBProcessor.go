package main

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/satori/go.uuid"
	"log"
	"os"
	"strings"
	"time"
)

//get region from deployment
var region = os.Getenv("AWS_REGION")

//   stream record data type
type Record struct {
	Client            string `json:"Client"`
	LogID             string `json:"LogId"`
	SourceType        string `json:"SourceType"`
	UserID            string `json:"UserId"`
	UserEmail         string `json:"UserEmail"`
	CreatedAt         string `json:"CreatedAt"`
	SourceID          string `json:"SourceId"`
	ChangeDescription string `json:"ChangeDescription"`
	ChangeData        string `json:"ChangeData"`
	IPAddress         string `json:"IPAddress"`
	Action            string `json:"Action"`
	ActionType        string `json:"ActionType"`
}

// xml format
// to convert string to xml
type XMLData struct {
	Id                 string `xml:"Id"`
	Client             string `xml:"Client"`
	RequestedUserId    string `xml:"RequestedUserId"`
	RequestedUserEmail string `xml:"RequestedUserEmail"`
	RequesterIpAddress string `xml:"RequesterIpAddress"`
	OriginatingUri     string `xml:"OriginatingUri"`
	WorkerId           string `xml:"WorkerId"`
	SearchTypes        Type   `xml:"SearchTypes"`
}

type Type struct {
	SearchType string `xml:"string"`
}

// format of data in database
type FormatData struct {
	Id                string `json "Id"`
	LogId             string `json:"LogId"`
	SourceType        string `json:"SourceType"`
	UserId            string `json:"UserId"`
	UserEmail         string `json:"UserEmail"`
	CreatedAT         int64  `json:"CreatedAT"`
	SourceId          string `json:"SourceId"`
	ChangeDescription string `json:"ChangeDescription"`
	ChangeData        string `json:"ChangeData"`
	IPAddress         string `json:"IPAddress"`
	Action            string `json:"Action"`
	ActionType        string `json:"ActionType"`
}

// Slice of Record
type SliceRecord struct {
	items []FormatData
}

var clientTables []string

// kinesis trigger
func DBProcessor(ctx context.Context, event events.KinesisEvent) {
	sess := session.New(&aws.Config{
		Region: aws.String(region),
	})

	svc := dynamodb.New(sess)
	// get kinesis stream event record
	for _, i := range event.Records {
		items := []FormatData{}
		recordSlice := SliceRecord{items}
		record := []Record{}
		formatData := FormatData{}
		if len(i.Kinesis.Data) > 0 {
			json.Unmarshal(i.Kinesis.Data, &record)
			//each partionkey(1-2000) belongs to a shard
			//each shard is responsible for each client
			for j := 0; j < len(record); j++ {
				record[j] = DataFormatter(record[j])
				uid, _ := uuid.NewV4()
				formatData.Id = uid.String()
				formatData.Action = record[j].Action
				formatData.ActionType = record[j].ActionType
				formatData.ChangeData = record[j].ChangeData
				formatData.ChangeDescription = record[j].ChangeDescription
				tempTime, err := time.Parse("2006-01-02 15:04:05", record[j].CreatedAt)
				if err != nil {
					log.Println(err)
				}
				formatData.CreatedAT = tempTime.Unix()
				formatData.IPAddress = record[j].IPAddress
				formatData.LogId = record[j].LogID
				formatData.SourceId = record[j].SourceID
				formatData.SourceType = record[j].SourceType
				formatData.UserEmail = record[j].UserEmail
				formatData.UserId = record[j].UserID
				recordSlice.AddItem(formatData)
				CheckStatus(svc, "Audit-Trail-"+record[j].Client, recordSlice.items)
			}
		}
	}
}

// list current table
// create table and write data to database
func CheckStatus(svc *dynamodb.DynamoDB, tableName string, items []FormatData) {
	// table is available
	if ListTables(svc, tableName) {
		log.Println("Writing items...")
		DbWrite(tableName, items, svc)
	} else {
		log.Println("Creating table...")
		if CreateTables(svc, tableName) {
			DbWrite(tableName, items, svc)
		}
	}
}

//check the client table if existed
func CheckClientTalbes(tableName string) bool {
	if len(clientTables) == 0 {
		return false
	} else {
		for _, i := range clientTables {
			if i == tableName {
				return true
			}
		}
	}
	return false
}

// list available table and check the client table if existed
// save listed tables in dynamoDB to search
func ListTables(svc *dynamodb.DynamoDB, tableName string) bool {
	if CheckClientTalbes(tableName) {
		return true
	} else {
		result, err := svc.ListTables(&dynamodb.ListTablesInput{})
		if err != nil {
			log.Println(err)
		}
		for _, i := range result.TableNames {
			tablename := *i
			clientTables = append(clientTables, tablename)
		}
		if CheckClientTalbes(tableName) {
			return true
		}
	}
	return false
}

// describe table and wait the created table active
func WaitTableActive(svc *dynamodb.DynamoDB, tableName string) bool {
	for true {
		result, err := svc.DescribeTable(&dynamodb.DescribeTableInput{
			TableName: aws.String(tableName),
		})
		if err != nil {
			log.Println(err)
		}
		if aws.StringValue(result.Table.TableStatus) == "ACTIVE" {
			return true
		} else {
			time.Sleep(10 * time.Millisecond)
		}
	}
	return true
}

// Create Client database table
func CreateTables(svc *dynamodb.DynamoDB, tableName string) bool {
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Id"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("CreatedAT"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Id"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("CreatedAT"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(tableName),
	}

	_, err := svc.CreateTable(input)
	if err != nil {
		log.Println(err)
	}
	return WaitTableActive(svc, tableName)
}

// write to database
func DbWrite(table string, items []FormatData, svc *dynamodb.DynamoDB) {

	for _, j := range items {
		clientItems, err := dynamodbattribute.MarshalMap(j)
		if err != nil {
			panic(err)
		}

		// Create item in table device
		input := &dynamodb.PutItemInput{
			Item:      clientItems,
			TableName: aws.String(table),
		}

		// Save structed data to dynamodb
		if _, err := svc.PutItem(input); err != nil {
			panic(err)
		}
	}
}

//format data
//convernt xml to string for changeDescritionEvent and format date yyyy-mm-dd hh:mm:ss
func DataFormatter(record Record) Record {
	// change utf-16 to utf-8
	if strings.Contains(record.ChangeDescription, "<") && strings.Contains(record.ChangeDescription, ">") {
		record.ChangeDescription = strings.Replace(record.ChangeDescription, "utf-16", "utf-8", 1)
		splitByComma := strings.Split(record.ChangeDescription, ",")
		splitData := strings.Split(splitByComma[2], ":")
		var result string
		for i := 1; i < len(splitData); i++ {
			result = result + splitData[i]
		}
		// convert xml to string remove xml structure
		xmlData := XMLData{}
		xml.Unmarshal([]byte(result), &xmlData)
		record.ChangeDescription = splitByComma[0] + splitByComma[1] + " Id:" + xmlData.Id + " Client:" + xmlData.Client + " OriginatingUri:" + xmlData.OriginatingUri + " RequestedUserEmail:" + xmlData.RequestedUserEmail + " RequestedUserId:" + xmlData.RequestedUserId + " RequesterIpAddress:" + xmlData.RequesterIpAddress + " SearchTypes:" + xmlData.SearchTypes.SearchType + " WorkerId:" + xmlData.WorkerId
		record.ChangeData = record.ChangeDescription
	}
	//format date
	tempDate, err := time.Parse("2006-01-02T15:04:05Z", record.CreatedAt)
	if err != nil {
		log.Print(err)
	}
	record.CreatedAt = tempDate.Format("2006-01-02 15:04:05")
	record.CreatedAt = record.CreatedAt
	return record
}

// function to additem to slice
func (recordSlice *SliceRecord) AddItem(item FormatData) []FormatData {
	recordSlice.items = append(recordSlice.items, item)
	return recordSlice.items
}

func main() {
	lambda.Start(DBProcessor)
}
