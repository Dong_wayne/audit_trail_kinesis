package main

import (
	//	"github.com/aws/aws-lambda-go/lambda"
	//	"context"
	//	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/athena"
	"log"
	//	"os"
)

//var region = os.Getenv("AWS_REGION")
var region = "ap-southeast-2"

func CreateTalbes() {
	sess := session.New(&aws.Config{
		Region: aws.String(region),
	})

	svc := athena.New(sess)

	CreateDatabase(svc)
	CreateTable(svc)
//	PrepareStatus(svc)
	LoadPartition(svc)
}

// create database if not exists
func CreateDatabase(svc *athena.Athena) {
	var query athena.StartQueryExecutionInput
	query.SetQueryString("CREATE DATABASE IF NOT EXISTS audit_trail_wayne LOCATION 's3://asseticwayneaudittrail/'")
	var result athena.ResultConfiguration
	result.SetOutputLocation("s3://asseticwayneaudittrail/")
	query.SetResultConfiguration(&result)
	_, err := svc.StartQueryExecution(&query)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Database was created.")
	}
}

// create athena table with partition(client and createdate)
func CreateTable(svc *athena.Athena) {
	var query athena.StartQueryExecutionInput
	query.SetQueryString("CREATE EXTERNAL TABLE IF NOT EXISTS `table_audit_trail_wayne`(`logid` string COMMENT 'from deserializer', `sourcetype` string COMMENT 'from deserializer', `userid` string COMMENT 'from deserializer', `useremail` string COMMENT 'from deserializer', `createdat` string COMMENT 'from deserializer', `sourceid` string COMMENT 'from deserializer', `changedescription` string COMMENT 'from deserializer', `changedata` string COMMENT 'from deserializer', `ipaddress` string COMMENT 'from deserializer', `action` string COMMENT 'from deserializer', `actiontype` string COMMENT 'from deserializer') PARTITIONED BY (`client` string, `createdate` string) ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' LOCATION 's3://asseticwayneaudittrail/test/'")
	var queryContext athena.QueryExecutionContext
	queryContext.SetDatabase("audit_trail_wayne")
	query.SetQueryExecutionContext(&queryContext)
	var result athena.ResultConfiguration
	result.SetOutputLocation("s3://asseticwayneaudittrail/")
	query.SetResultConfiguration(&result)
	_, err := svc.StartQueryExecution(&query)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Table was created.")
	}
}

func PrepareStatus(svc *athena.Athena) {
	var query athena.StartQueryExecutionInput
	query.SetQueryString("MSCK REPAIR TABLE `table_audit_trail_wayne`")
	var queryContext athena.QueryExecutionContext
	queryContext.SetDatabase("audit_trail_wayne")
	query.SetQueryExecutionContext(&queryContext)
	var result athena.ResultConfiguration
	result.SetOutputLocation("s3://asseticwayneaudittrail/")
	query.SetResultConfiguration(&result)
	_, err := svc.StartQueryExecution(&query)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Prepare.")
	}
}

func LoadPartition(svc *athena.Athena) {
	var query athena.StartQueryExecutionInput
	query.SetQueryString("ALTER TABLE `table_audit_trail_wayne` ADD PARTITION (`client`='tester3',`createdate`='2018-09-09') LOCATION 's3://asseticwayneaudittrail/test/client=tester3/createdate=2018-09-09'")
	var queryContext athena.QueryExecutionContext
	queryContext.SetDatabase("audit_trail_wayne")
	query.SetQueryExecutionContext(&queryContext)
	var result athena.ResultConfiguration
	result.SetOutputLocation("s3://asseticwayneaudittrail/")
	query.SetResultConfiguration(&result)
	_, err := svc.StartQueryExecution(&query)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Load Partition.")
	}
}

func main() {
	//	lambda.Start(CreateTalbes)
	CreateTalbes()
}
