#!/bin/sh
# Import and deploy API
API_ID=$(aws apigateway import-rest-api --body 'file://src/main/IngestAPI.json' --region ap-southeast-2 --query id --output text)
aws apigateway create-deployment --rest-api-id $API_ID --stage-name dev --description 'Deploy an IngestAPI' --region ap-southeast-2
printf "Endpoint:https://$API_ID.execute-api.ap-southeast-2.amazonaws.com/dev/AuditTrail/{stream-name}\n"
printf "API Deployed Sucessfully\n"