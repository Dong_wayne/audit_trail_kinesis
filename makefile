#Go command line
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get

# Paths
BIN=src/main/bin/
SRC=src/main/

# Outputs
OUTPUT= RecordProcessor JsonRecordProcessor

.PHONY: all
#all: deps test build
all: deps build

$(OUTPUT): %: $(SRC)/%.go
	GOOS=linux $(GOBUILD) -o $(BIN)$@ $<

build: $(OUTPUT)

.PHONY: test
test:
	$(GOTEST) -v ./...

.PHONY: clean
clean:
	$(GOCLEAN)
	rm -rf $(BIN)

.PHONY: deps
deps:
	$(GOGET) -u github.com/aws/aws-lambda-go/lambda
	$(GOGET) -u github.com/aws/aws-sdk-go
	$(GOGET) -u github.com/satori/go.uuid
	$(GOGET) -u github.com/dsnet/compress/bzip2

# # deploy IngetstAPI
# id := $(shell ./deploy.sh)
# .PHONY: deploy
# deploy:
# 	@echo $(id)
